package com.op;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

public class MatrixManager implements MatrixHelper {
    int matrixSize, randomBound;

    public MatrixManager(int maxRandom) {
        this.randomBound = maxRandom;
    }

    public int getMatrixSize() {
        return matrixSize;
    }

    public void setMatrixSize(int matrixSize) {
        this.matrixSize = matrixSize;
    }

    @Override
    public Matrix generate() {
        Random random = new Random();
        int[][] mat = new int[matrixSize][matrixSize];
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                mat[i][j] = random.nextInt(this.randomBound);
            }
        }
        return new Matrix(mat, matrixSize);
    }

    @Override
    public Multiplication[] getMultiplications(Matrix matrix1, Matrix matrix2) {
        ArrayList<Multiplication> multiplications = new ArrayList<>();
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                for (int k = 0; k < matrixSize; k++) {
                    int[] values = new int[]{matrix1.getValues()[i][k], matrix2.getValues()[k][j]};
                    int finalI = i;
                    int finalJ = j;
                    Optional<Multiplication> multiplication =
                            multiplications.stream().filter(m -> finalI == m.getRow() && finalJ == m.getColumn()).findAny();
                    if (multiplication.isPresent()) {
                        multiplication.get().extend(values);
                    } else {
                        multiplications.add(new Multiplication(i, j, values));
                    }
                }
            }
        }
        return multiplications.toArray(new Multiplication[matrixSize * matrixSize]);
    }
}
