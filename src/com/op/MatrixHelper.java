package com.op;

public interface MatrixHelper {
    Matrix generate();
    Multiplication[] getMultiplications(Matrix matrix1, Matrix matrix2);
}
