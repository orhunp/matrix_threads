package com.op;

import java.util.Locale;

public class Matrix {
    int[][] values;
    int size;

    public Matrix(int[][] values, int size) {
        this.values = values;
        this.size = size;
    }

    public int[][] getValues() {
        return values;
    }

    public int getSize() {
        return size;
    }

    public void print() {
        System.out.printf(Locale.US, "Printing matrix (%1$dx%1$d):\n", this.getSize());
        for (int i = 0; i < this.getSize(); i++) {
            for (int j = 0; j < this.getSize(); j++)
                System.out.print(this.getValues()[i][j] + " ");
            System.out.println();
        }
        System.out.println("===");
    }
}
