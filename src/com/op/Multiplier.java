package com.op;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Semaphore;

public class Multiplier {

    static long testMultiplyMatrix(MatrixManager matrixManager, int threadCount, boolean rendezvous, boolean quiet) {

        Date startTime = Calendar.getInstance().getTime();

        Matrix matrix1 = matrixManager.generate();
        Matrix matrix2 = matrixManager.generate();
        if (!quiet) {
            matrix1.print();
            matrix2.print();
        }
        Multiplication[] multiplications = matrixManager.getMultiplications(matrix1, matrix2);

        ArrayList<Thread> threads = new ArrayList<>();
        ArrayList<Multiplication[]> workers = new ArrayList<>();
        int chunks = (int) Math.ceil((double) multiplications.length / threadCount);
        for (int i = 0; i < multiplications.length; i += chunks) {
            workers.add(Arrays.copyOfRange(multiplications, i, Math.min(multiplications.length, i + chunks)));
        }
        Semaphore semaphore = new Semaphore(1);

        for (int i = 0; i < workers.size(); i++) {
            int finalI = i;
            threads.add(new Thread(() -> {
                if (rendezvous) {
                    try {
                        semaphore.acquire();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                for (Multiplication multiplication : workers.get(finalI)) {
                    multiplication.calculate();
                }
                if (rendezvous) {
                    semaphore.release();
                }
            }));
            threads.get(i).start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int[][] result = new int[matrixManager.getMatrixSize()][matrixManager.getMatrixSize()];
        for (Multiplication multiplication : multiplications) {
            result[multiplication.getRow()][multiplication.getColumn()] = multiplication.getResult();
        }
        Matrix matrix3 = new Matrix(result, matrixManager.getMatrixSize());

        Date endTime = Calendar.getInstance().getTime();
        long elapsed = endTime.getTime() - startTime.getTime();
        if (!quiet) {
            matrix3.print();
        }
        return elapsed;
    }
}
