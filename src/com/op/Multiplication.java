package com.op;

import java.util.ArrayList;

public class Multiplication {
    ArrayList<int[]> calculations = new ArrayList<>();
    int row, column, result;

    public Multiplication(int row, int column, int[] values) {
        this.row = row;
        this.column = column;
        this.calculations.add(values);
    }

    public void extend(int[] values) {
        this.calculations.add(values);
    }

    public void calculate() {
        for (int[] values: calculations) {
            this.result += values[0] * values[1];
        }
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public int getResult() {
        return result;
    }
}
