package com.op;

public class Main {

    private static final int MAX_RANDOM = 20;

    public static void main(String[] args) {
        MatrixManager matrixManager = new MatrixManager(MAX_RANDOM);

        for (int matrixSize: new int[] {3, 10, 100, 1000}) {
            for (int threadCount : new int[]{1, 2, 4, 8}) {
                matrixManager.setMatrixSize(matrixSize);
                System.out.printf("%1$dx%1$d matrix multiplication with %2$d threads...", matrixManager.getMatrixSize(), threadCount);
                long elapsed = Multiplier.testMultiplyMatrix(matrixManager, threadCount, false, true);
                System.out.printf("%dms\n", elapsed);
            }
        }
    }
}
